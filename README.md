# FTM MEDEXPERT
Expert system of medicine use OWL API library.

## Feedback

* Ask a question on [site](https://ftm.pm).

## License

Copyright (c) FTM PM LLC. All rights reserved.

Licensed under the [MIT](LICENSE.txt) License.
