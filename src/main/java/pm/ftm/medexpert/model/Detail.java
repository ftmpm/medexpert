package pm.ftm.medexpert.model;

import lombok.Data;

@Data
public class Detail implements Query {
    private String name;
}
