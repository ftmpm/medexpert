package pm.ftm.medexpert.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ResultCollection<T> implements Result {
    List<T> collection = new ArrayList<>();

    public void add(T item) {
        collection.add(item);
    }

    public void remove(T item) {
        collection.remove(item);
    }
}
