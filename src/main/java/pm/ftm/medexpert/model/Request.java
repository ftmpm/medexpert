package pm.ftm.medexpert.model;

import lombok.Data;

@Data
public class Request {
    private Query query;
}
