package pm.ftm.medexpert.model;

import java.io.Serializable;

import lombok.Data;

@Data
public final class Token implements Serializable {
    private String lexeme;
    private int tag;
}
