package pm.ftm.medexpert.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Diagnosis implements Query {
    private List<String> symptoms = new ArrayList<>();
    private List<String> negativeSymptoms = new ArrayList<>();
    private List<String> contraindications = new ArrayList<>();
    private int age;
    private boolean sex;
}
