package pm.ftm.medexpert.model;

import lombok.Data;

import java.util.List;

@Data
public class DiagnosisResult implements Result {
    private List<String> symptoms;
    private List<String> contraindications;
    private List<String> diseases;
    private List<String> drugs;
    private List<String> questionSymptom;
    private String prediction;

    public DiagnosisResult() {
    }

    public DiagnosisResult(List<String> symptoms, List<String> contraindications, List<String> diseases, List<String> drugs, String prediction) {
        this.symptoms = symptoms;
        this.contraindications = contraindications;
        this.diseases = diseases;
        this.drugs = drugs;
        this.prediction = prediction;
    }
}
