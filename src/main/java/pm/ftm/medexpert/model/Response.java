package pm.ftm.medexpert.model;

import lombok.Data;

@Data
public class Response {
    private String error;
    private Result body;

    public Response(Result body) {
        this.body = body;
    }

    public Response(Exception exception) {
        this.error = exception.getMessage();
    }

    public Response(String error) {
        this.error = error;
    }
}
