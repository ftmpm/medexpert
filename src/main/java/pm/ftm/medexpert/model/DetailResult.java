package pm.ftm.medexpert.model;

import lombok.Data;

@Data
public class DetailResult extends Detail implements Result {
    private String info;
}
