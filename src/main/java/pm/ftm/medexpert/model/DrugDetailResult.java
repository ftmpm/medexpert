package pm.ftm.medexpert.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DrugDetailResult implements Result {
    private String name;
    private String link;
    private String description;
    private String price;
    private List<String> cure = new ArrayList<>();
    private List<String> contraindications = new ArrayList<>();
    private String methodOfApplication;
    private String interaction;

    public void addCure(String item) {
        cure.add(item);
    }
    public void addContraindication(String item) {
        contraindications.add(item);
    }
}
