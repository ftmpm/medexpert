package pm.ftm.medexpert.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pm.ftm.medexpert.handler.OntologyHandler;
import pm.ftm.medexpert.model.Diagnosis;
import pm.ftm.medexpert.model.Detail;
import pm.ftm.medexpert.model.Drug;
import pm.ftm.medexpert.model.Response;

@RestController
public class ApiController {
    /**
     * The Ontology handler
     */
    private final OntologyHandler ontologyHandler;

    @Autowired
    public ApiController(OntologyHandler ontologyHandler) {
        this.ontologyHandler = ontologyHandler;
    }

    /**
     * @param diagnosis Request model
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/diagnostics", method = RequestMethod.POST, produces = "application/json")
    public Response diagnostics(@RequestBody Diagnosis diagnosis) {
        return ontologyHandler.getDiagnosis(diagnosis);
    }

    /**
     * @param detail Request model
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/details", method = RequestMethod.POST, produces = "application/json")
    public Response details(@RequestBody Detail detail) {
        return ontologyHandler.getDetail(detail);
    }

    /**
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/symptoms", method = RequestMethod.GET, produces = "application/json")
    public Response symptoms() {
        return ontologyHandler.getSymptoms();
    }

    /**
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/contraindications", method = RequestMethod.GET, produces = "application/json")
    public Response contraindications() {
        return ontologyHandler.getContraindications();
    }

    /**
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/diseases", method = RequestMethod.GET, produces = "application/json")
    public Response diseases() {
        return ontologyHandler.getDiseases();
    }

    /**
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/drugs", method = RequestMethod.POST, produces = "application/json")
    public Response drugs(@RequestBody Drug drug) {
        return ontologyHandler.getDrugs(drug);
    }

    /**
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/drugs-all", method = RequestMethod.GET, produces = "application/json")
    public Response drugsAll() {
        return ontologyHandler.getDrugsAll();
    }

    /**
     * @return Return response
     */
    @CrossOrigin
    @RequestMapping(value = "/api/diseases-list", method = RequestMethod.POST, produces = "application/json")
    public Response diseasesList(@RequestBody Detail detail) {
        return ontologyHandler.getDiseasesList(detail);
    }
}