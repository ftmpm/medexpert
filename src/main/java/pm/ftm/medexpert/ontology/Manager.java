package pm.ftm.medexpert.ontology;

import org.semanticweb.HermiT.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.StringDocumentSource;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import pm.ftm.medexpert.model.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public final class Manager implements OntologyLoader, QueryReasoner {
    private OWLOntology ontology;
    private OWLOntologyManager ontologyManager;
    private OWLDataFactory factory;
    private OWLReasoner reasoner;
    private String prefix = "#";

    public Manager() {
        ontologyManager = OWLManager.createOWLOntologyManager();
    }

    @Override
    public boolean load(String ontologyIRI) throws IOException {
        String ontologyString = this.getOntologyContentByOntologyIRI(ontologyIRI);

        return this.loadFromString(ontologyString);
    }

    @Override
    public boolean loadFromString(String ontologyIRI) {
        try {
            ontology = ontologyManager.getOntology(IRI.create("4a68aff2-7a83-4885-bae1-b1ee7855cdb0"));
            // need to check the changes and if there are no changes, do not reload the ontology, now just reset
            if (ontology == null) {
                ontology = ontologyManager.loadOntologyFromOntologyDocument(new StringDocumentSource(ontologyIRI));
                prefix = "#";
            }
            init();

        } catch (OWLOntologyCreationException exception) {
            ontology = null;
        }

        return loaded();
    }

    @Override
    public boolean loadFromFile(File file) throws OWLOntologyCreationException {
        if (ontology != null) {
            ontologyManager.removeOntology(ontology);
            ontology = null;
            prefix = "#";
        }
        ontology = ontologyManager.loadOntologyFromOntologyDocument(file);
        init();

        return loaded();
    }


    @Override
    public boolean loaded() {
        return ontology != null;
    }

    @Override
    public ResultCollection<String> diseasesList(Detail detail) {
        OWLNamedIndividual drugNamedIndividual = factory.getOWLNamedIndividual(IRI.create(prefix + detail.getName()));
        OWLObjectProperty cureProperty = factory.getOWLObjectProperty(IRI.create(prefix + "лечение"));
        List<String> diseases = reasoner.objectPropertyValues(drugNamedIndividual, cureProperty)
                .map(item -> item.getIRI().getShortForm()).sorted().collect(Collectors.toList());

        ResultCollection<String> response = new ResultCollection<>();
        response.setCollection(diseases);

        return response;
    }

    @Override
    public ResultCollection<Result> drugs(Drug drugInfo) {
        List<OWLNamedIndividual> diseases = drugInfo.getDiseases().stream()
                .map(disease -> factory.getOWLNamedIndividual(IRI.create(prefix + disease)))
                .collect(Collectors.toList());

        List<OWLNamedIndividual> contraindications = drugInfo.getContraindications().stream()
                .map(disease -> factory.getOWLNamedIndividual(IRI.create(prefix + disease)))
                .collect(Collectors.toList());

        OWLObjectProperty cureProperty = factory.getOWLObjectProperty(IRI.create(prefix + "лечение"));
        OWLObjectProperty contraindicationProperty = factory.getOWLObjectProperty(IRI.create(prefix + "противопоказан"));
        OWLClass drugClass = factory.getOWLClass(IRI.create(prefix + "препарат"));
        List<OWLNamedIndividual> drugsInstances = reasoner.instances(drugClass).sorted().collect(Collectors.toList());
        List<OWLNamedIndividual> drugsAll = new ArrayList<>();
        ResultCollection<Result> response = new ResultCollection<>();
        for (OWLNamedIndividual drug : drugsInstances) {
            reasoner.objectPropertyValues(drug, cureProperty).filter(diseases::contains).forEach(item -> {
                Long count = reasoner.objectPropertyValues(drug, contraindicationProperty)
                        .filter(contraindications::contains).count();
                if (!drugsAll.contains(drug) && count == 0) {
                    drugsAll.add(drug);
                    Detail drugReq = new Detail();
                    drugReq.setName(drug.getIRI().getShortForm());
                    Result drugDetailResult = detail(drugReq);
                    response.add(drugDetailResult);
                }
            });
        }

        return response;
    }

    @Override
    public ResultCollection<Result> drugsAll() {
        OWLClass drugClass = factory.getOWLClass(IRI.create(prefix + "препарат"));
        List<OWLNamedIndividual> drugsInstances = reasoner.instances(drugClass).sorted().collect(Collectors.toList());

        ResultCollection<Result> response = new ResultCollection<>();
        for (OWLNamedIndividual drug : drugsInstances) {
            Detail drugReq = new Detail();
            drugReq.setName(drug.getIRI().getShortForm());
            Result drugDetailResult = detail(drugReq);
            response.add(drugDetailResult);
        }

        return response;
    }

    @Override
    public DiagnosisResult diagnostics(Diagnosis diagnosis) {
        List<OWLNamedIndividual> symptoms = diagnosis.getSymptoms().stream()
                .map(symptom -> factory.getOWLNamedIndividual(IRI.create(prefix + symptom)))
                .collect(Collectors.toList());
        List<OWLNamedIndividual> negativeSymptoms = diagnosis.getNegativeSymptoms().stream()
                .map(symptom -> factory.getOWLNamedIndividual(IRI.create(prefix + symptom)))
                .collect(Collectors.toList());

        OWLObjectProperty symptomDiseaseProperty = factory.getOWLObjectProperty(IRI.create(prefix + "синптом-заболевания"));
        OWLClass diseaseClass = factory.getOWLClass(IRI.create(prefix + "заболевание"));
        Set<OWLNamedIndividual> diseasesAll = new HashSet<>();
        Set<OWLNamedIndividual> diseases = new HashSet<>();
        List<OWLNamedIndividual> diseaseInstances = reasoner.instances(diseaseClass).collect(Collectors.toList());

        for (OWLNamedIndividual symptom : symptoms) {
            for (OWLNamedIndividual subjectIndividual : diseaseInstances) {
                Long postitiveCount = reasoner.objectPropertyValues(subjectIndividual, symptomDiseaseProperty)
                        .filter(objectIndividual -> objectIndividual.equals(symptom)).count();
                if (postitiveCount > 0) {
                    diseasesAll.add(subjectIndividual);
                }
            }
        }

        if(!negativeSymptoms.isEmpty()) {
            for (OWLNamedIndividual negativeSymptom : negativeSymptoms) {
                for (OWLNamedIndividual disease : diseasesAll) {
                    Long negativeCount = reasoner.objectPropertyValues(disease, symptomDiseaseProperty).filter(objectIndividual -> objectIndividual.equals(negativeSymptom)).count();
                    if (negativeCount == 0) {
                        diseases.add(disease);
                    }
                }
            }

            diseasesAll = new HashSet<>(diseases);
            diseases.clear();
        }

        List<String> diseasesResponse = new ArrayList<>();
        List<String> symptomsResponse = new ArrayList<>();
        Map<OWLNamedIndividual, Integer> rates = new HashMap<>();

        int symptomsCount = symptoms.size();
        for (OWLNamedIndividual disease : diseasesAll) {
            Long count = reasoner.objectPropertyValues(disease, symptomDiseaseProperty).filter(symptoms::contains).count();
            if (symptomsCount == count) {
                diseasesResponse.add(disease.getIRI().getShortForm());
                diseases.add(disease);
                reasoner.objectPropertyValues(disease, symptomDiseaseProperty).forEach(item -> {
                    if (!symptomsResponse.contains(item.getIRI().getShortForm())) {
                        rates.put(item, 0);
                        symptomsResponse.add(item.getIRI().getShortForm());
                    } else {
                        rates.merge(item, 1, Integer::sum);
                    }
                });
            }
        }

        String prediction = null;
        if (diseases.size() > 1) {
            int maxValue = Collections.max(rates.values());
            if(maxValue > 1) {
                do {
                    maxValue--;
                    for (Map.Entry<OWLNamedIndividual, Integer> entry : rates.entrySet()) {
                        if (entry.getValue() == maxValue && !negativeSymptoms.contains(entry.getKey()) && !symptoms.contains(entry.getKey())) {
                            prediction = entry.getKey().getIRI().getShortForm();
                            break;
                        }
                    }
                } while (prediction == null && maxValue >= 0);
            } else {
                for (Map.Entry<OWLNamedIndividual, Integer> entry : rates.entrySet()) {
                    if (entry.getValue() == maxValue && !negativeSymptoms.contains(entry.getKey()) && !symptoms.contains(entry.getKey())) {
                        prediction = entry.getKey().getIRI().getShortForm();
                        break;
                    }
                }
            }

        }

        DiagnosisResult response = new DiagnosisResult();
        Collections.sort(diseasesResponse);
        Collections.sort(symptomsResponse);
        response.setDiseases(diseasesResponse);
        response.setSymptoms(symptomsResponse);
        response.setPrediction(prediction);

        return response;
    }

    @Override
    public Result detail(Detail detail) {
        DrugDetailResult response = new DrugDetailResult();
        OWLNamedIndividual detailItem = factory.getOWLNamedIndividual(IRI.create(prefix + detail.getName()));

        OWLDataProperty descriotionProperty = factory.getOWLDataProperty(IRI.create(prefix + "описание"));
        OWLDataProperty priceProperty = factory.getOWLDataProperty(IRI.create(prefix + "цена"));
        OWLDataProperty linkProperty = factory.getOWLDataProperty(IRI.create(prefix + "ссылка"));
        OWLDataProperty interactionProperty = factory.getOWLDataProperty(IRI.create(prefix + "взаимодействие"));
        OWLDataProperty methodOfApplicationProperty = factory.getOWLDataProperty(IRI.create(prefix + "cпособ-применения"));
        OWLObjectProperty cureProperty = factory.getOWLObjectProperty(IRI.create(prefix + "лечение"));
        OWLObjectProperty contraindicationProperty = factory.getOWLObjectProperty(IRI.create(prefix + "противопоказан"));

        response.setName(detail.getName());
        reasoner.dataPropertyValues(detailItem, descriotionProperty).forEach(item -> response.setDescription(item.getLiteral()));
        reasoner.dataPropertyValues(detailItem, priceProperty).forEach(item -> response.setPrice(item.getLiteral()));
        reasoner.dataPropertyValues(detailItem, linkProperty).forEach(item -> response.setLink(item.getLiteral()));
        reasoner.dataPropertyValues(detailItem, interactionProperty).forEach(item -> response.setInteraction(item.getLiteral()));
        reasoner.dataPropertyValues(detailItem, methodOfApplicationProperty).forEach(item -> response.setMethodOfApplication(item.getLiteral()));
        reasoner.objectPropertyValues(detailItem, cureProperty).forEach(item -> response.addCure(item.getIRI().getShortForm()));
        reasoner.objectPropertyValues(detailItem, contraindicationProperty).forEach(item -> response.addContraindication(item.getIRI().getShortForm()));

        return response;
    }

    @Override
    public ResultCollection<String> symptoms()  {
        ResultCollection<String> response = new ResultCollection<>();
        OWLClass symptomClass = factory.getOWLClass(IRI.create(prefix + "синптом"));
        List<String> res = reasoner.instances(symptomClass).map(item-> item.getIRI().getShortForm()).sorted().collect(Collectors.toList());
        response.setCollection(res);

        return response;
    }

    @Override
    public ResultCollection<String> contraindications()  {
        ResultCollection<String> response = new ResultCollection<>();
        OWLClass contraindicationClass = factory.getOWLClass(IRI.create(prefix + "противопоказание"));
        List<String> res = reasoner.instances(contraindicationClass).map(item-> item.getIRI().getShortForm()).sorted().collect(Collectors.toList());
        response.setCollection(res);

        return response;
    }

    @Override
    public ResultCollection<String> diseases()  {
        ResultCollection<String> response = new ResultCollection<>();
        OWLClass diseaseClass = factory.getOWLClass(IRI.create(prefix + "заболевание"));
        List<String> res = reasoner.instances(diseaseClass).map(item-> item.getIRI().getShortForm()).sorted().collect(Collectors.toList());
        response.setCollection(res);


        return response;
    }

    private void init() {
        factory = ontologyManager.getOWLDataFactory();
        reasoner = new ReasonerFactory().createReasoner(ontology);
        prefix = ontology.getOntologyID().getOntologyIRI().map(iri -> iri  + prefix).orElseGet(() -> prefix);
    }

    /**
     * Return ontology content from ontologyIRI
     *
     * @param ontologyIRI Ontology IRI
     * @return the ontology
     * @throws IOException Ontology wasn't loaded
     */
    private String getOntologyContentByOntologyIRI(String ontologyIRI) throws IOException {
        URL url = new URL(ontologyIRI);
        StringBuilder stringBuilder = new StringBuilder();

        try (InputStreamReader isr = new InputStreamReader(url.openStream(), "UTF-8")) {
            char[] buffer = new char[1024];
            int count;
            while ((count = isr.read(buffer, 0, 1024)) != -1) {
                stringBuilder.append(new String(buffer, 0, count));
            }
        }
        return stringBuilder.toString();
    }
}
