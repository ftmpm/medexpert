package pm.ftm.medexpert.ontology;

import pm.ftm.medexpert.model.*;

public interface QueryReasoner {
    /**
     * Get response on question
     *
     * @param diagnosis The query
     * @return Reasoner answer
     */
    DiagnosisResult diagnostics(Diagnosis diagnosis);

    /**
     * Get response on question
     *
     * @param detail The query
     * @return Reasoner answer
     */
    Result detail(Detail detail);

    /**
     * @return Reasoner answer
     */
    ResultCollection<String> symptoms();

    /**
     * @return Reasoner answer
     */
    ResultCollection<String> diseases();

    /**
     * @return Reasoner answer
     */
    ResultCollection<Result> drugs(Drug drugInfo);

    /**
     * @return Reasoner answer
     */
    ResultCollection<Result> drugsAll();

    /**
     * @return Reasoner answer
     */
    ResultCollection<String> diseasesList(Detail detail);

    /**
     * @return Reasoner answer
     */
    ResultCollection<String> contraindications();
}
