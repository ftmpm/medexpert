package pm.ftm.medexpert.ontology;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import java.io.File;
import java.io.IOException;

public interface OntologyLoader {
    /**
     * Load ontology by IRI
     *
     * @param ontologyIRI Path to ontology
     * @return Is loaded
     * @throws IOException Throw error
     */
    boolean load(String ontologyIRI) throws IOException;

    /**
     * Load ontology from string
     *
     * @param ontology The ontology
     * @return Is loaded
     */
    boolean loadFromString(String ontology);

    /**
     * Load ontology from file
     *
     * @param file File with ontology
     * @return Is loaded
     */
    boolean loadFromFile(File file) throws OWLOntologyCreationException;

    /**
     * @return Is load ontology
     */
    boolean loaded();
}
