package pm.ftm.medexpert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedexpertApplication {
    /**
     * Main
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(MedexpertApplication.class, args);
    }
}
